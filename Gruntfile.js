module.exports = function(grunt){
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        // typescript: {
        //     userscript: {
        //         src:  ['src/main.ts'],
        //         dest: 'compiled/main.js',
        //         options: {
        //             target: 'es5'
        //         }
        //     }
        // },

        shell: {
            tsc: {
                command: 'tsc'
            }
        },

        concat:{
            guard: {
                files:{
                    'compiled/guard.js': [
                        'etc/intro.txt',
                        'compiled/main.js',
                        'etc/outro.txt'
                    ]
                }
            },
            finalize: {
                files: {
                    'dist/witches_tea_party.user.js': [
                        'etc/globals.txt',
                        'compiled/guard.js'
                    ],
                    'dist/witches_tea_party.meta.js': []
                },
                options: {
                    banner: grunt.file.read('etc/header.txt')
                }
            }
        }
  
    });

    // grunt.loadNpmTasks('grunt-typescript');
    grunt.loadNpmTasks('grunt-shell');
    grunt.loadNpmTasks('grunt-contrib-concat');

    // grunt.registerTask('default', ['typescript', 'concat:guard', 'concat:finalize']);
    grunt.registerTask('default', ['shell', 'concat:guard', 'concat:finalize']);
}
