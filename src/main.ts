/* 汎用関数型 */
type Mapping<T, R> = (x: T) => R
type Predicate<T> = (x: T) => boolean
type Supplier<T> = () => T
type Consumer<T> = (x: T) => void
type Routine = () => void
type BinaryOperator<T> = (lhs: T, rhs: T) => T
type UnaryOperator<T> = (x: T) => T

type Nullish = null | undefined

type TargetSite = {
    urlRegex: RegExp,
    resSelecter: string,
    hasRedTruthList: boolean,
    getResNo: Mapping<JQuery<HTMLElement>, number>
    postTask: Consumer<JQuery<HTMLElement>>
}

function isNotNullish<T>(x: T): x is Exclude<T, Nullish> {
    return x != null
}

function format(strings: TemplateStringsArray, ...keys: number[]) {
    return (function(...values: string[]) {
        const result = [strings[0]];
        keys.forEach(function(key, i) {
            const value = values[key];
            result.push(value, strings[i + 1]);
        });
        return result.join('');
    })
}

/* Optional Mapping 風処理 */
class Unit<T> {
    $$: T
    constructor(x: T) {
        this.$$ = x
    }
    map<R>(f: Mapping<T, R>) {
        return new Unit(f(this.$$))
    }
    forEach(f: Consumer<T>) {
        f(this.$$)
    }
}
function $$<T>(x: T) {
    if (isNotNullish(x)) {
        return new Unit(x)
    } else {
        return null
    }
}

/* 定数群 */

const RED_TRUTH_REGEX = /【(.*?)】/gs
const BLUE_TRUTH_REGEX = /『(.*?)』/gs
const RED_TRUTH_CLASS = 'red-truth'
const BLUE_TRUTH_CLASS = 'blue-truth'
const PRE_RED_TRUTH = '【'
const POST_RED_TRUTH = '】'
const PRE_BLUE_TRUTH = '『'
const POST_BLUE_TRUTH = '』'
const DST_FORMAT = format `<span class="${0}"><span style="font-size: 0;">${1}</span>$1<span style="font-size: 0;">${2}</span></span>`
const RED_TRUTH_DST = DST_FORMAT(RED_TRUTH_CLASS, PRE_RED_TRUTH, POST_RED_TRUTH)
const BLUE_TRUTH_DST = DST_FORMAT(BLUE_TRUTH_CLASS, PRE_BLUE_TRUTH, POST_BLUE_TRUTH)

const GAIBUITA_ITA: TargetSite = {
    urlRegex: /jbbs\.shitaraba\.net\/internet\/3775/,
    resSelecter: 'table.thread>tbody>tr>td.text>dd>span',
    hasRedTruthList: false,
    getResNo: $x => parseInt($x.parent('dd').prev('dt').children('span.name').children('a').eq(0).text()),
    postTask: function($x) {},
}
const GAIBUITA_THREAD: TargetSite = {
    urlRegex: /jbbs\.shitaraba\.net\/bbs\/read\.cgi\/internet\/3775\/\d+\/?.*/,
    resSelecter: '#thread-body>dd',
    hasRedTruthList: true,
    getResNo: $x => parseInt($x.prev('dt').children('a').eq(0).text()),
    postTask: function($x) {
        /* 置き換え時にレス先の内容をポップアップする処理が外れるので自前で付け直す */

        /* アンカー */
        $x.children('span.res').children('a').hover(function() {
            $(this).siblings('dl.rep-comment').show()
            return false
        }, function() {
            $(this).siblings('dl.rep-comment').hide()
            return false
        })

        /* ポップアップ */
        $x.children('span.res').children('dl.rep-comment').hover(function() {
            $(this).show()
            return false
        }, function() {
            $(this).hide()
            return false
        })
    },
}
const TARGET_SITES = [GAIBUITA_ITA, GAIBUITA_THREAD, ]

const CSS_STRING = [
    ':root {',
    '    --red: #DE424C;',
    '    --blue: #219DDD;',
    '}',
    '',
    '.red-truth {',
    '    color: var(--red);',
    '}',
    '',
    '.blue-truth {',
    '    color: var(--blue);',
    '}',
].join('\n')

/* /定数群 */

function firstText($obj: JQuery<HTMLElement>) {
    const elem = $($obj[0].outerHTML)
    elem.children().empty()
    return elem.text()
}

function getNowSite(document: Document) {
    for (const site of TARGET_SITES) {
        if (site.urlRegex.test(document.location.href)) {
            return  site
        }
    }
    return null
}

function extractRedTruth($res: JQuery<HTMLElement>, nowSite: TargetSite | Nullish) {
    const redTruths: [number, string][] = []
    const resNum = nowSite?.getResNo($res) ?? NaN
    const resText = firstText($res)
    let result: RegExpExecArray | null
    while (true) {
        result = RED_TRUTH_REGEX.exec(resText)
        if (isNotNullish(result)) {
            redTruths.push([resNum, result[1]])
        } else {
            return redTruths
        }
    }
}

function colourTruths($res: JQuery<HTMLElement>) {
    const element = $res[0]
    const src = element.innerHTML
    const dst = src
        .replace(RED_TRUTH_REGEX, RED_TRUTH_DST)
        .replace(BLUE_TRUTH_REGEX, BLUE_TRUTH_DST)
    element.innerHTML = dst
}

/* 実行内容 */
$(function() {
    const nowSite = getNowSite(this)

    const $style = $('<style />').attr('type', 'text/css').html(CSS_STRING)
    $('head').eq(0).append($style)
    
    let redTruths: [number, string][] = []

    /* レス本文ごとに回す */
    const $reses = $$ (nowSite)?.map(x => $(x.resSelecter)). $$
    $reses?.each(function() {
        const $res = $(this)

        redTruths = redTruths.concat(extractRedTruth($res, nowSite))
        colourTruths($res)
    })

    /* 赤き真実のリスト,今後実装予定 */
    if (nowSite?.hasRedTruthList) {
        /* TODO: 正式な実装 */
        let resMin = NaN
        let resMax = NaN
        let isBetweenResRange: Predicate<[number, string]> = x => !(x[0] < resMin) && !(x[0] > resMax)

        let enabledList = redTruths.filter(isBetweenResRange)
        enabledList.forEach(x => console.log(x)) // 代替としてデバッグコンソールに表示しておく
    }

    /* サイトごとの後処理 */
    $$ ($reses)?.forEach(x => nowSite?.postTask(x))// $$
})
